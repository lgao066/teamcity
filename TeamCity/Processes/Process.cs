﻿using System;
using OpenQA.Selenium;
using TeamCity.Pages;

namespace TeamCity.Processes
{
    public class Process
    {
        // Member variables
        public IWebDriver driver { get; set; }
        private Page Page;

        // Constructor
        public Process() { }
        public Process(IWebDriver d)
        {
            // Initiate all data
            driver = d;

            // Initiate all pages
            Page = new Page(driver);
        }

        private void naviToUrlProcess()
        {
            Page.openBrowserAndGoToUrl();
        }

        public void signIn(string username, string password)
        {
            naviToUrlProcess();
            Page.login(username, password);
            Page.assertLoginSuccessfully();
        }
    }
}

