﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace TeamCity.Pages
{
    public class Page
    {
        public IWebDriver driver;

        public Page(IWebDriver d)
        {
            driver = d;
            PageFactory.InitElements(driver, this);
        }

        public void openBrowserAndGoToUrl()
        {
            driver.Navigate().GoToUrl("http://sandbox.clinicwise.net/");
            driver.Manage().Window.Maximize();
        }

        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement UserName_text;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement Password_text;

        [FindsBy(How = How.Id, Using = "signin_button")]
        public IWebElement Signin_button;

        // 
        public void login(string username, string password)
        {
            UserName_text.SendKeys(username);
            Password_text.SendKeys(password);
            Signin_button.Click();
        }

        public void assertLoginSuccessfully()
        {
            bool elementCheck = driver.FindElement(By.XPath("//div[@id ='flash_notice']")).Displayed;
            Assert.IsTrue(elementCheck, "Element was not present");
        }
    }
}

