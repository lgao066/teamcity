﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Chrome;
using TeamCity.Processes;

namespace TeamCity.Test
{
    [TestFixture]
    [Parallelizable]
    public class Test
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            //driver = new ChromeDriver();
            driver = new ChromeDriver("C:\\Webdrivers");

            //DesiredCapabilities cap = DesiredCapabilities.Firefox();

            // RemoteWebDriver
            //Uri url = new Uri("http://localhost:4444/wd/hub");
            //driver = new RemoteWebDriver(url, cap);

            // This is to test the merge!!!???

        }

        [Test]
        public void TestOne()
        {
            Process p = new Process(driver);
            p.signIn("admin", "test");

            //driver.Url("http://google.com");
            //System.out.println(driver.getTitle());
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Close();
        }
    }
}

